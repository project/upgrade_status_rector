<?php

namespace Drupal\upgrade_status_rector\Form;

use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UpgradeStatusRectorForm extends FormBase {

  /**
   * The list of available modules.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Rector result storage.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $rectorResults;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('extension.list.module'),
      $container->get('keyvalue')
    );
  }

  /**
   * Constructs a \Drupal\upgrade_status_rector\UpgradeStatusRectorForm.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   The module extension list service.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   The key/value factory.
   */
  public function __construct(
    ModuleExtensionList $module_extension_list,
    KeyValueFactoryInterface $key_value_factory
  ) {
    $this->moduleExtensionList = $module_extension_list;
    $this->rectorResults = $key_value_factory->get('upgrade_status_rector_results');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drupal_upgrade_status_rector_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $list = [];
    $modules = $this->moduleExtensionList->reset()->getList();
    foreach ($modules as $key => $module) {
      if ($module->origin === 'core' || $module->status === 0) {
        // Ignore core modules and disabled modules.
        continue;
      }
      if ($results = $this->rectorResults->get($module->getName())) {
        $form[$key] = [
          '#type' => 'details',
          '#closed' => 'true',
          '#title' => $this->t('Results for @module', ['@module' => $module->getName()])
        ];
        $form[$key]['results'] = [
          '#type' => 'textarea',
          '#value' => $this->formatResults($results, $module)
        ];
      }
      $list[$key] = $module->getName();
    }
    $form['module'] = [
      '#title' => $this->t('Pick module to run rector on'),
      '#type' => 'select',
      '#options' => $list,
      '#weight' => -10,
    ];
    $form['run'] = [
      '#type' => 'submit',
      '#value' => $this->t('Run rector'),
      '#weight' => -5,
    ];
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $vendor_path = $this->findVendorPath();
    $module_machine_name = $form_state->getValue('module');
    $module = $this->moduleExtensionList->get($module_machine_name);

    $output = [];
    $cmd = 'cd ' . dirname($vendor_path) . ' && ' . $vendor_path . '/bin/rector process ' . DRUPAL_ROOT . '/' . $module->getPath() . ' --dry-run --config=' . dirname(dirname(__DIR__)) . '/rector.yml';
    exec($cmd, $output);

    $output = join("\n", $output);
    $this->rectorResults->set($module->getName(), $output);

    if (strpos($output, '[OK] Rector is done!')) {
      $this->messenger()->addMessage($this->t('Parsing @module was successful.', ['@module' => $module->getName()]));
    }
    else {
      $this->messenger()->addError($this->t('Error while parsing @module.', ['@module' => $module->getName()]));
    }
  }

  /**
   * Reformats the rector output string as a combined diff.
   *
   * @param string $string
   *   Rector output string.
   *
   * @return string
   *   Reformatted output as diff if the rector output was successful. The
   *   verbatim output otherwise.
   */
  private function formatResults($string, $module) {
    if (strpos($string, '[OK] Rector is done!')) {
      // If this was a successful run, reformat as patch. This rector version
      // does not have an output format option yet.
      $lines = explode("\n", $string);
      $state = 'throw';
      $file = '';
      foreach ($lines as $num => &$line) {
        if ($state == 'throw') {
          if (preg_match('!^\d+\) (.+)!', $line, $found)) {
            $file = str_replace(DRUPAL_ROOT . '/' . $module->getPath() . '/', '', $found[1]);
            unset($lines[$num]);
            $state = 'seeking diff';
          }
          else {
            // Keep throwing away lines until we find a file.
            unset($lines[$num]);
          }
        }
        if ($state == 'seeking diff') {
          if ($line != "    ---------- begin diff ----------") {
            unset($lines[$num]);
          }
          else {
            $line = 'Index: ' . $file;
            $state = 'diff';
          }
        }
        if ($state == 'diff') {
          if ($line == '--- Original') {
            $line = '--- a/' . $file;
          }
          elseif ($line == '+++ New') {
            $line = '+++ b/' . $file;
          }
          if ($line == '    ----------- end diff -----------') {
            $state = 'throw';
            unset($lines[$num]);
          }
        }
      }
      return join("\n", $lines);
    }
    return $string;
  }

  /**
   * Finds vendor location.
   *
   * @return string|null
   *   Vendor directory path if found, null otherwise.
   */
  protected function findVendorPath() {
    // The vendor directory may be found inside the webroot (unlikely).
    if (file_exists(DRUPAL_ROOT . '/vendor/bin/rector')) {
      return DRUPAL_ROOT . '/vendor';
    }
    // Most likely the vendor directory is found alongside the webroot.
    elseif (file_exists(dirname(DRUPAL_ROOT) . '/vendor/bin/rector')) {
      return dirname(DRUPAL_ROOT) . '/vendor';
    }
    $this->logger('upgrae_status_rector')->error('Rector executable not found');
  }

  /**
   * Sample code to test ourselves for now.
   */
  private function sampleCode() {
    $foo = LOCALE_PLURAL_DELIMITER;
    drupal_set_message('bar');
  }

}
